var palavras_dicas = {
    nacionalidade:["romeno","americano","turco","russo"],
    frutas:["banana","abacaxi","laranja"],
    esporte:["futebol","golfe","beisebol","vôlei"]
};
let tentativas = 0;
var nacionalidade = ["romeno","americano","belga","russo", "espanhol","colombiano","canadense"];
var display = document.querySelector(".palavra");
var palavraEscondida = nacionalidade[Math.floor(Math.random() * nacionalidade.length)]

var buttons = document.querySelectorAll(".btn");
var boneco = document.querySelector(".boneco");
retornarPalavra();

var letrasDigitadas = [];
buttons.forEach((btn,index)=>{
    btn.addEventListener("click",(e)=>{
        if(verificarTentaivas(e.target.textContent, index)){
        }
    })
})
function verificarTentaivas(letra, index){
    if(tentativas < 6){
        if(!letrasDigitadas.includes(letra)){
            letrasDigitadas.push(letra);
            verificarPalavra(letra,index);
        }
    }else{
        alert("fim de jogo");
    }
   
}
function retornarPalavra(){
    for(let i = 0; i < palavraEscondida.length; i++){
        var span = document.createElement('span');
        display.appendChild(span);
    }
}

function verificarPalavra(letra, index){
    if(palavraEscondida.split("").includes(letra)){
        buttons[index].classList.add("palavraCorreta");
        display.children[palavraEscondida.split("").indexOf(letra)].textContent = letra;

    }else{
        buttons[index].classList.add("palavraErrada");
        aparecerBoneco();

    }
}

function aparecerBoneco(){

    boneco.children[tentativas].classList.add("aparecer");
    tentativas++;
}